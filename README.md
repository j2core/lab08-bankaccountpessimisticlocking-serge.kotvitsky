You are creating simple bank accounts service, backed by the database.
In that database you have to create 10 accounts with names account1, account2 etc. and $100,000 balance each.
You need to run 10 concurrent threads for every account (so, total 100 concurrent thread started at the same time).
Every of threads should make 1000 transactions sequentially,decreasing the balance of his account onto $10 in the database per transaction.
In the end of such run, balance of every account have to be zero.
Use pessimistic locking for concurrency synchronization while using DB.
